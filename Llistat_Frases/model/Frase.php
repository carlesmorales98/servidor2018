<?php


class Frase{

  private $nom;
  private $categoria;
  private $frase;

  public function __construct($n, $c, $f){
    $this-> nom = $n;
    $this-> categoria = $c;
    $this-> frase = $f;

  }

  public function getNom(){
    return $this->nom;
  }

  public function getCategoria(){
    return $this->categoria;
  }

  public function getFrase(){
    return $this->frase;
  }

}
