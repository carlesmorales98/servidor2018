<?php 

require_once(__DIR__.'/../GameC.php');

class GameCDb{

    private $conn;

    public function createGameC($gamec){
        //Open connection
        $this->openConnection();

        //Prepare statement
        $insert = "INSERT INTO GameC (name, specie, attackP, defenseP) VALUES (?, ?, ?, ?)";
        $stmt = $this->conn->prepare($insert);

        //Binding params
        $stmt->bind_param("ssii", $n, $s, $a, $d);
        $n = $gamec->getName();
        $s = $gamec->getSpecie();
        $a = $gamec->getAttackP();
        $d = $gamec->getDefenseP();

        //Execute
        $stmt->execute();

        return true;
    }

    public function listGameC(){
        $this->openConnection();
        $query = "SELECT * FROM GameC";
        $stmt = $this->conn->prepare($query);

        $stmt->execute();
        $res = $stmt->get_result();

        $result = array();

        //Processing results
        while ($gc = $res->fetch_assoc() ) {
            array_push($result, new GameC($gc['name'], $gc['specie']));
        }
        return $result;
    }

    public function getGameC(){
        $this->openConnection();
        $query = "SELECT * FROM GameC WHERE gcid = ? ";
        $stmt = $this->conn->prepare($query);
        $stmt->bind_param("i", $id);
        $id = 1;


        $stmt->execute();
        $res = $stmt->get_result();

        $result = null;

        //Processing results
        while ($gc = $res->fetch_assoc() ) {
            $result = new GameC($gc['name'], $gc['specie']);
        }
        return $result;
    }


    private function openConnection () {
        if($this->conn == null){
            $this->conn = mysqli_connect("127.0.0.1",
                                        "game",
                                        "cfgs!DAW2018",
                                        "game");
        }
    }

}
